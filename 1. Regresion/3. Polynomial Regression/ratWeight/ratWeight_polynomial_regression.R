# Polynomial Regression

# Importing the dataset
dataset = read.csv('ratWeight.csv')

library(caTools)

set_B38625 = subset(dataset, dataset$id == 'B38625')

library(ggplot2)
theme_set(theme_bw())
pl <- ggplot(set_B38625) + geom_point(aes(x=weight, y=week), size=2, colour="#993399") + 
  xlab("Weight") + ylab("week")  
print(pl)

lm0 <- lm(set_B38625$week ~ 1)
lm0

mean(set_B38625$weight)
mean(set_B38625$week)

pl + geom_hline(yintercept=coef(lm0)[1],size=1, colour="#339900")

# Fitting Linear Regression to the dataset
lin_reg = lm(formula = weight ~ week,
             data = set_B38625)
summary(lin_reg)

lm1 <- lm(weight ~ week, data=set_B38625)
coef(lm1)
summary(lm1)

par(mfrow = c(1, 2))
plot(lm1, which=c(1,2))

# Fitting Polynomial Regression to the dataset
set_B38625$weight2 = set_B38625$weight^2
set_B38625$weight3 = set_B38625$weight^3
set_B38625$weight4 = set_B38625$weight^4
poly_reg = lm(formula = weight ~ week + weight2 + weight3 + weight4,
              data = set_B38625)

# Visualising the Linear Regression results
# install.packages('ggplot2')
library(ggplot2)
ggplot() +
  geom_point(aes(x = set_B38625$weight, y = set_B38625$week),
             colour = 'red') +
  geom_line(aes(x = set_B38625$weight, y = predict(lin_reg, newdata = set_B38625)),
            colour = 'blue') +
  ggtitle('Truth or Bluff (Linear Regression)') +
  xlab('Level') +
  ylab('Salary')

# Visualising the Polynomial Regression results
# install.packages('ggplot2')
library(ggplot2)
ggplot() +
  geom_point(aes(x = set_B38625$weight, y = set_B38625$week),
             colour = 'red') +
  geom_line(aes(x = set_B38625$weight, y = predict(poly_reg, newdata = set_B38625)),
            colour = 'blue') +
  ggtitle('Truth or Bluff (Polynomial Regression)') +
  xlab('Level') +
  ylab('Salary')

