# Simple Linear Regression

# Importing the dataset
dataset = read.csv('Poverty.txt', sep = '\t')

library(caTools)

# Fitting Simple Linear Regression to the Training set
regressor = lm(formula =  Brth15to17 ~ PovPct,
               data = dataset)

# Predicting the Test set results
y_pred = predict(regressor, newdata = dataset)

# Visualising the Training set results
library(ggplot2)
ggplot() +
  geom_point(aes(x = dataset$PovPct, y = dataset$Brth15to17),
             colour = 'red') +
  geom_line(aes(x = dataset$PovPct, y_pred),
            colour = 'blue') +
  ggtitle('PovPct vs Brth15to17') +
  xlab('PovPct') +
  ylab('Brth15to17')

summary(regressor)
