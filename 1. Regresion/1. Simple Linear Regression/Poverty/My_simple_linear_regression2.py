# Simple Linear Regression

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def simple_linear_regression (x,y,x_label = "X",y_label = "Y"):
    
    # Fitting Simple Linear Regression to the Training set
    from sklearn.linear_model import LinearRegression
    regressor = LinearRegression()
    regressor.fit(x, y)
    
    # Predicting the Test set results
    y_pred = regressor.predict(x)
    
    # Visualising the Training set results
    plt.scatter(x, y, color = 'red')
    plt.plot(x, y_pred, color = 'blue')
    plt.title('%s vs %s' % (x_label, y_label))
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.show()
    
    # The coefficients
    from sklearn.metrics import mean_squared_error, r2_score
    
    print('Coefficients: \n', regressor.coef_)
    print('Intercept: \n', regressor.intercept_)
    # The mean squared error
    print("Mean squared error: %.2f" % mean_squared_error(y, y_pred))
    # Explained variance score: 1 is perfect prediction
    print("Variance score: %.2f" % r2_score(y, y_pred))
# -----------------------------------------------    

# Importing the dataset with tab seperator
dataset = pd.read_csv('Poverty.txt', sep='\t')

X_PovPct = dataset.iloc[:, 1:2].values
y_Brth15to17 = dataset.iloc[:, 2].values
y_Brth18to19 = dataset.iloc[:, 3].values
y_ViolCrime = dataset.iloc[:, 4].values
y_TeenBrth = dataset.iloc[:, 5].values

simple_linear_regression(X_PovPct, y_Brth15to17, "X_PovPct", "y_Brth15to17")
# answer should be y = 4.267 + 1.373x

simple_linear_regression(X_PovPct, y_Brth15to17, "X_PovPct", "y_Brth15to17")
simple_linear_regression(X_PovPct, y_Brth18to19, "X_PovPct", "y_Brth18to19")
simple_linear_regression(X_PovPct, y_ViolCrime, "X_PovPct", "y_ViolCrime")
simple_linear_regression(X_PovPct, y_TeenBrth, "X_PovPct", "y_TeenBrth")
