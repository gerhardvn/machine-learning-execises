# Simple Linear Regression

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def simple_linear_regression (x,y):
    # Splitting the dataset into Training set and Test set
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X_PovPct, y_Brth15to17, test_size = 1/3,
                                                        random_state = 0)
    
    # Fitting Simple Linear Regression to the Training set
    from sklearn.linear_model import LinearRegression
    regressor = LinearRegression()
    regressor.fit(X_train, y_train)
    
    # Predicting the Test set results
    y_pred = regressor.predict(X_test)
    
    
    # Visualising the Training set results
    plt.scatter(X_train, y_train, color = 'red')
    plt.plot(X_train, regressor.predict(X_train), color = 'blue')
    plt.title('Poverty Percentage vs Births 15 - 17 (Training set)')
    plt.xlabel('Poverty')
    plt.ylabel('Births ')
    plt.show()
    
    # Visualising the Test set results
    plt.scatter(X_test, y_test, color = 'red')
    plt.plot(X_train, regressor.predict(X_train), color = 'blue')
    plt.title('Poverty Percentage vs Births 15 - 17  (Test set)')
    plt.xlabel('Poverty')
    plt.ylabel('Births')
    plt.show()
    
    # The coefficients
    from sklearn.metrics import mean_squared_error, r2_score
    
    print('Coefficients: \n', regressor.coef_)
    print('Intercept: \n', regressor.intercept_)
    # The mean squared error
    print("Mean squared error: %.2f" % mean_squared_error(y_test, y_pred))
    # Explained variance score: 1 is perfect prediction
    print('Variance score: %.2f' % r2_score(y_test, y_pred))
# -----------------------------------------------    

# Importing the dataset
dataset = pd.read_csv('Poverty.txt', sep='\t')

X_PovPct = dataset.iloc[:, 1:2].values
y_Brth15to17 = dataset.iloc[:, 2].values

simple_linear_regression(X_PovPct, y_Brth15to17)