# Simple Linear Regression

# Importing the dataset
dataset = read.csv(unzip('kc_house_data.zip','kc_house_data.csv'))


library(caTools)

set.seed(123)
split = sample.split(dataset$price, SplitRatio = 2/3)
training_set = subset(dataset, split == TRUE)
test_set = subset(dataset, split == FALSE)

# Fitting Simple Linear Regression to the Training set
regressor = lm(formula =  price ~ sqft_living,
               data = training_set)

# Predicting the Test set results
y_pred1 = predict(regressor, newdata = training_set)
y_pred2 = predict(regressor, newdata = test_set)

# Visualising the Training set results
library(ggplot2)
ggplot() +
  geom_point(aes(x = test_set$sqft_living, y = test_set$price),
             colour = 'red') +
  geom_line(aes(x = test_set$sqft_living, y_pred2),
            colour = 'blue') +
  ggtitle('sqft_living vs price') +
  xlab('sqft_living') +
  ylab('price')

library(ggplot2)
ggplot() +
  geom_point(aes(x = training_set$sqft_living, y = training_set$price),
             colour = 'red') +
  geom_line(aes(x = training_set$sqft_living, y_pred1),
            colour = 'blue') +
  ggtitle('sqft_living vs price') +
  xlab('sqft_living') +
  ylab('price')

summary(regressor)
