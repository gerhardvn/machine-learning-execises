# Simple Linear Regression
# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def simple_linear_regression (x,y,x_label = "X",y_label = "Y"):
    
    # Fitting Simple Linear Regression to the Training set
    from sklearn.linear_model import LinearRegression
    regressor = LinearRegression()
    regressor.fit(x, y)
    
    # Predicting the Test set results
    y_pred = regressor.predict(x)
    
    # Visualising the Training set results
    plt.scatter(x, y, color = 'red')
    plt.plot(x, y_pred, color = 'blue')
    plt.title('%s vs %s' % (x_label, y_label))
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.show()
    
    # The coefficients
    from sklearn.metrics import mean_squared_error, r2_score
    
    print('Coefficients: \n', regressor.coef_)
    print('Intercept: \n', regressor.intercept_)
    # The mean squared error
    print("Mean squared error: %.2f" % mean_squared_error(y, y_pred))
    # Explained variance score: 1 is perfect prediction
    print("Variance score: %.2f" % r2_score(y, y_pred))
# -----------------------------------------------    

# Importing the dataset with tab seperator
dataset = pd.read_fwf('fev_dat.txt', widths =[3,7,5,4,6])

datasubset = dataset[(dataset.age >= 6) & (dataset.age <= 10)]

age = datasubset.age.values.reshape(-1, 1)
fev = datasubset.FEV.values.reshape(-1, 1)
ht = datasubset.ht.values.reshape(-1, 1)
sex = datasubset.sex.values.reshape(-1, 1)
smoke = datasubset.smoke.values.reshape(-1, 1)

simple_linear_regression(age, fev, "age", "FEV")
# FEV = 0.01165 + 0.26721 × age

simple_linear_regression(ht, fev, "ht", "FEV")
