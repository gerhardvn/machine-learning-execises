Linear regresion execises
=========================

Simple Linear Regression
------------------------

https://newonlinecourses.science.psu.edu/stat462/node/101/  
1. Teen Birth Rate and Poverty Level Data: Poverty.txt
2. Lung Function in 6 to 10 Year Old Children: Fev_Data.txt

https://college.cengage.com/mathematics/brase/understandable_statistics/7e/students/datasets/slr/frames/frame.html   	
1. List Price Vs. Best Price for a New GMC Pickup: slr01.xls 
2. Cricket Chirps Vs. Temperature: slr02.xls
3. Diameter of Sand Granules Vs. Slope on Beach: slr03.xls
4. National Unemployment Male Vs. Female: slr04.xls
5. Fire and Theft in Chicago: slr05.xls
6. Auto Insurance in Sweden: slr06.xls
7. Gray Kangaroos: slr07.xls
8. Pressure and Weight in Cryogenic Flow Meters: slr08.xls
9. Ground Water Survey: slr09.xls
10. Iris Setosa: slr10.xls
11. Pizza Franchise: slr11.xls
12. Prehistoric Pueblos: slr12.xls

https://www.kaggle.com/mayanksrivastava/predict-housing-prices-simple-linear-regression  
1. Predict Housing Prices: kc_house_data.csv

Multiple Linear Regression
------------------------
https://newonlinecourses.science.psu.edu/stat462/node/129/
1. Example on IQ and Physical Characteristics

https://www.kaggle.com/mayanksrivastava/predict-housing-prices-simple-linear-regression  
1. Predict Housing Prices: kc_house_data.csv

Polynomial Regression
---------------------
http://sia.webpopix.org/regression_PC.html
1. rat weights measured over 14 weeks: ratWeight.csv

Classification Execise
======================

Logistic Regresion
------------------

https://www.kaggle.com/dileep070/anomaly-detection
1. Credit card fraud detection


